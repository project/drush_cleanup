
-- SUMMARY --

drush_cleanup depends on drush (the DRUpal SHell).
To use drush_cleanup, you must first download and install drush from
http://drupal.org/project/drush. Installation instructions can be found at
http://tinyurl.com/drush-installation, which also contains a section about
installing other commands (such as drush_cleanup).

For a full description of the module, visit the project page:
  http://drupal.org/project/drush_cleanup

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/drush_cleanup


-- REQUIREMENTS --

Drush 3.x (http://drupal.org/project/drush)


-- INSTALLATION --

You can put this folder in a number of places:
- In a .drush folder in your HOME folder. Note, that you have to make the
  .drush folder yourself (so you end up with ~/.drush/drush_cleanup/INSTALL.txt).
- In a folder specified with the include option (see above).
- In /path/to/drush/commands (not a Smart Thing, but it would work).


This project has been sponsored by:
* XTND.US
  Provider of Drupal & jQuery plug-ins for Adobe Dreamweaver & Eclipse IDE.